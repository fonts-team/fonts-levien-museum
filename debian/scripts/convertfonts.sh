#!/bin/sh

CONVERTFONT=$(pwd)/debian/scripts/ConvertFont.ff
SRC="./"
TARGET="TTF"
test ! -d ${TARGET} && mkdir ${TARGET}

FONTS=`cd ${SRC} && find . -name "*.sfd"`
for font in ${FONTS}; do
	echo $font
	(cd ${TARGET} && ${CONVERTFONT} ../${SRC}/${font} )
done
